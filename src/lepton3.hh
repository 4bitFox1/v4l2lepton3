/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#pragma once

#include "spi.hh"
#include <string>
#include <mutex>
#include <thread>
#include <chrono>
#include <atomic>
#include <condition_variable>

class Lepton3
{
protected:
	const std::string &spiDeviceName;

	uint8_t *segmentBufferCurrent   = nullptr;  //[SEGMENT_SIZE][PACKET_SIZE]
	uint8_t *segmentBufferOld       = nullptr;  //[SEGMENT_SIZE][PACKET_SIZE]
	uint8_t *frameBufferCurrent     = nullptr;  //[FRAME_SIZE][SEGMENT_SIZE][PACKET_DATA_SIZE]
	uint8_t *frameBufferOld         = nullptr;  //[FRAME_SIZE][SEGMENT_SIZE][PACKET_DATA_SIZE]

	static constexpr uint32_t FrameSize = 4;
	static constexpr uint32_t SegmentSize = 60;
	static constexpr uint32_t PacketDataSize = 160;
	static constexpr uint32_t PacketSize = PacketDataSize + 4;

	std::mutex frameBufferMutex;
	bool frameIsRequested = false;
	std::condition_variable frameReady;

	std::thread captureThread;
	std::atomic<bool> captureOn{false};

public:
	static constexpr uint32_t FrameWidth = 160;
	static constexpr uint32_t FrameHeight = 120;
	static constexpr uint32_t FrameBufferSize = FrameSize * SegmentSize * PacketDataSize;

    Lepton3(const std::string &spiDeviceName);
    ~Lepton3();

    Lepton3(const Lepton3 &) = delete;
    Lepton3& operator=(const Lepton3 &) = delete;

    void StartCapture();
	void StopCapture();
	const uint8_t* GetFrame(int64_t timeoutMs = 5000);

protected:
	void Capture();
    bool ReadSegment(const SPI& spi);
};

