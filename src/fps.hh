/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#pragma once

#include <chrono>
#include <cstdint>

class FpsCounter
{
public:

	FpsCounter(uint32_t reportIntervalMs = 1000) : reportIntervalMs{reportIntervalMs}, startTime{std::chrono::steady_clock::now()} {}
	void Tick();


protected:
	uint32_t frameCounter = 0;
	uint32_t reportIntervalMs = 0;
	std::chrono::time_point<std::chrono::steady_clock> startTime;
};